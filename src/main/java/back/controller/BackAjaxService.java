package back.controller;

import global.db.DefaultProperty;
import global.mappin.Result;
import global.memcach.Cach;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javassist.bytecode.stackmap.TypeData.ClassName;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.whalin.MemCached.MemCachedClient;

import back.dao.IBackDAO;

/**
 * @author hua
 * 後台API控制器
 */
@Controller
public class BackAjaxService {
	
	private static final long serialVersionUID = 1L;
	
	private static Logger logger = Logger.getLogger(ClassName.class);
	
	//系統屬性
	@Autowired
	protected DefaultProperty defaultProperty;	
	
	@Autowired
	private IBackDAO backDAO;
	
	//memcached 存取
	@Autowired
	private Cach cach;
	
	/**
	 * 取詞句分類資料
	 * @param session
	 * @return Result
	 */
	@RequestMapping(value="/sentence/category" , method = RequestMethod.GET)
	public Result getSentenceCategory(HttpSession session){
		Result result = new Result();		
		result.data = backDAO.getSentenceCategory(defaultProperty.getLang(), true);
		if(result.data != null){
			result.success = true;
		}
		return result;
	}
	
	
	/**
	 * 取詞句資料列表 (具分頁功能)
	 * @param skip
	 * @param count
	 * @param find		搜尋條件 (JSON 字串)
	 * @param sort		排序條件  (JSON 字串)
	 * @param session
	 * @return Result
	 */
	@RequestMapping(value="/sentence/paging" , method = RequestMethod.GET)
	public Result pagingSentence(
			@RequestParam(value = "skip")int skip,
			@RequestParam(value = "count")int count,
			@RequestParam(value="find") String find,
			@RequestParam(value="sort") String sort,
			HttpSession session) {
		
		Result result = new Result();
		result.data = backDAO.pagingSentence(skip, count, find,  sort, true);
		if(result.data != null){
			result.success = true;
		}
		return result;
	}

	
	/** 插入一筆詞句資料
	 * @param content		英文   		not null
	 * @param context_tw	繁中   		null
	 * @param context_cn	簡中   		null
	 * @param sound			發聲檔路徑	null
	 * @param icon			小圖示路徑	null		
	 * @param category		種類			not null
	 * @param session
	 * @return Result
	 */
	@RequestMapping(value="/sentence/insert" , method = RequestMethod.POST)
	public Result insertSentence(
			@RequestParam(value = "content")String content, 
			@RequestParam(value = "context_tw")String context_tw,
			@RequestParam(value = "context_cn")String context_cn,
			@RequestParam(value = "sound")String sound,
			@RequestParam(value = "icon")String icon,
			@RequestParam(value = "category")String category,	
			HttpSession session) {
		
		//取回memcached資料
		MemCachedClient mcc = cach.getCacheClient();
		String sessionID = session.getId();
		Map cachResource = (HashMap)mcc.get(sessionID) ;		
		

		
		//TODO 需要加上驗證字串
		//session 的做法
		//Map<String, Object> user = (Map<String, Object>)session.getAttribute(defaultProperty.getsUserKey());
		Map<String, Object> user = (Map<String, Object>)cachResource.get( defaultProperty.getsUserKey() );
		
		ObjectId userId = new ObjectId(user.get("id").toString());
		
		Result result = new Result();
		Map param = new HashMap<String, Object>();
		param.put("content", content);
		param.put("context_tw", context_tw);
		param.put("context_cn", context_cn);
		param.put("sound", sound);
		param.put("icon", icon);
		param.put("category", category);		
		param.put("createID", userId);		
		param.put("creationDate", new Date());
		param.put("modifiedUser", userId);
		param.put("modifiedDate", new Date());
		param.put("enable", true);
		
		result.success = backDAO.insertSentence(param);		
		return result;
	}	
	
	@RequestMapping(value="/sentence/update" , method = RequestMethod.POST)
	public Result updateSentence(
			@RequestParam(value = "id")String id,
			@RequestParam(value = "content")String content, 
			@RequestParam(value = "context_tw")String context_tw,
			@RequestParam(value = "context_cn")String context_cn,
			@RequestParam(value = "sound")String sound,
			@RequestParam(value = "icon")String icon,
			@RequestParam(value = "category")String category,
			HttpSession session){
		
		//取回memcached資料
		MemCachedClient mcc = cach.getCacheClient();
		String sessionID = session.getId();
		Map cachResource = (HashMap)mcc.get(sessionID) ;	
		
		//userID session
		//Map<String, Object> user = (Map<String, Object>)session.getAttribute(defaultProperty.getsUserKey());
		Map<String, Object> user = (Map<String, Object>)cachResource.get( defaultProperty.getsUserKey() );
		ObjectId userId = new ObjectId(user.get("id").toString());			
		
		Map param = new HashMap<String, Object>();
		param.put("content", content);
		param.put("context_tw", context_tw);
		param.put("context_cn", context_cn);
		param.put("sound", sound);
		param.put("icon", icon);
		param.put("category", category);		
		param.put("modifiedUser", userId);
		param.put("modifiedDate", new Date());
		
		Result result = new Result();
		result.success = backDAO.updateSentence(id, param);	
		return result;
	}
}
