package back.controller;

import global.controller.ControllerBase;
import javassist.bytecode.stackmap.TypeData.ClassName;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author hua
 * 後台頁面控制器
 */
@Controller
@RequestMapping("/back")
public class BackController extends ControllerBase{	
	
	private static Logger logger = Logger.getLogger(ClassName.class);
    
    /**
     * 開啟管理頁 /back/management
     * @param model
     * @return ModelAndView
     */
    @RequestMapping(value= "/management", method=RequestMethod.GET)
    public ModelAndView  dispatcherManagementPage(HttpSession session){
    	logger.info("dispatcher back.management");
    	
    	ModelAndView view =  new ModelAndView("error");    	
    	boolean hasPriority = super.resourceExist(session, "back/management");
    	if(hasPriority){
    		view.setViewName("back.management");  
    	}else{
    		view.addObject("errorMessage", getNoPermissions());
    	}
    	return view;
    }
    
    /**
     * 開啟資源管理頁 /back/res-management
     * @param model
     * @return ModelAndView
     */
    @RequestMapping(value= "/res-management", method=RequestMethod.GET)
    public ModelAndView  dispatcherResManagementPage(HttpSession session){
    	logger.info("dispatcher back.management.res");
    	
    	ModelAndView view =  new ModelAndView("error");   	
    	boolean hasPriority = super.resourceExist(session, "back/res-management");
    	if(hasPriority){
    		view.setViewName("back.management.res");  	
    	}else{
    		view.addObject("errorMessage", getNoPermissions());
    	}
    	return view;
    }    
    
    /**
     * 開啟編輯頁 /back/editor
     * @param model
     * @return ModelAndView
     */
    @RequestMapping(value= "/editor", method=RequestMethod.GET)
    public ModelAndView  dispatcherEditorPage(HttpSession session){
    	logger.info("dispatcher back.editor");
    	
    	ModelAndView view =  new ModelAndView("error");   	
    	boolean hasPriority = super.resourceExist(session, "back/editor");
    	if(hasPriority){
    		view.setViewName("back.editor");  	
    	}else{
    		view.addObject("errorMessage", getNoPermissions());
    	}    	
    	return view;
    }    
    
}
