package back.dao;

import global.db.CollectionNames;
import global.db.DefaultProperty;
import global.db.MongoConnection;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javassist.bytecode.stackmap.TypeData.ClassName;

import org.apache.log4j.Logger;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.util.JSON;

@Configuration
public class BackDao implements IBackDAO {
	
	private static Logger logger = Logger.getLogger(ClassName.class);
	
	//系統屬性
	@Autowired
	private DefaultProperty defaultProperty;	
	
	//資料表名稱
	@Autowired
	private CollectionNames collNames;
	
	//資料庫連線物件
	@Autowired	
	private MongoConnection mongoConnection;	
	
	
	//取詞句列表
	@Override
	public List<Map> getSentenceCategory(String lang, boolean enable){
		DB db = null;
		DBCollection coll = null;
		DBCursor cursor  = null;
		List<Map> sentenceCategory = null;
		
		try {					
			db = mongoConnection.getDB();
			coll = db.getCollection(collNames.getSentenceCategory());
			
			//查詢條件
			BasicDBObject query = new BasicDBObject();			
			query.put("enable", enable);
			
			//返回欄位
			BasicDBObject fields = new BasicDBObject();
			fields.put("name_" + lang, 1);
			fields.put("category", 1);
			fields.put("cname", 1);
			fields.put("order",1);			
		
			cursor = coll.find(query, fields).sort(new BasicDBObject("order",1)); //-1 降冪   1升冪	
			
			if(cursor.size() > 0){
				sentenceCategory = new ArrayList<Map>();
				while(cursor.hasNext()) {
					Map item = ((BasicDBObject)cursor.next()).toMap();
					String id = item.get("_id").toString();	//取回存String ID值
					item.remove("_id");							//移除ObjectId 
					item.put("id", id);					
					sentenceCategory.add( item );
				}	
			}			
		} catch (UnknownHostException e) {
			logger.error(e.toString());			
		}finally{
			if(cursor != null){
				cursor.close();				
			}
			db = null;
			coll = null;
			cursor = null;
		}		
		return sentenceCategory;
	}
	
	
	//新增詞句
	@Override
	public boolean insertSentence(Map<String, Object > param) {
		DB db = null;
		DBCollection coll = null;
		boolean success = false;
		
		try {					
			db = mongoConnection.getDB();
			coll = db.getCollection(collNames.getSentence());
			
			BasicDBObject document = new BasicDBObject(param);
			Random random = new Random();
			ArrayList matrix = new ArrayList();
			matrix.add(random.nextDouble());
			matrix.add(0);
			document.put("random", matrix);
			coll.insert(document);
			logger.info("insert" + document +"success");
			success = true;
			
		} catch (UnknownHostException e) {
			logger.error(e.toString());			
		}finally{
			db = null;
			coll = null;
		}		
		return success;
	}
	
	
	//更新詞句
	@Override
	public boolean updateSentence(String id ,Map<String, Object> param) {
		DB db = null;
		DBCollection coll = null;
		boolean success = false;
		
		try {					
			db = mongoConnection.getDB();
			coll = db.getCollection(collNames.getSentence());
			
			DBObject updatedValue = new BasicDBObject(param);			
	        DBObject updateSetValue = new BasicDBObject("$set",updatedValue);
			
	        DBObject query = new BasicDBObject();
			query.put("_id", new ObjectId(id));
			/* 
	         * 後兩參數是： 
	         *  1. 若是ture，更新document不存在，會插入。
	         *  2. 若是true，會同時更新多筆符合條件的document。 
	         */  
			coll.update(query, updateSetValue, false, false);
			logger.info("update sentence");
			success = true;
			
		} catch (UnknownHostException e) {
			logger.error(e.toString());			
		}finally{
			db = null;
			coll = null;
		}		
		return success;		
	}

	//關閉詞句
	@Override
	public boolean offSentence(String id) {
		// TODO Auto-generated method stub
		return false;
	}
	
	//
	@Override
	public Map findSentenceList(String find, String lang, boolean enable) {
		return null;		
	}
	
	//查詢詞句數量
	@Override
	public long getSentenceQuantity(String find, String category, int enable) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	//find json str , sort json str
	//取詞句列表
	@Override
	public List<Map> pagingSentence(int skip, int count, String find, String sort, boolean enable) {
		DB db = null;
		DBCollection coll = null;
		DBCursor cursor  = null;
		List<Map> sentenceList = null;
		
		try {					
			db = mongoConnection.getDB();
			coll = db.getCollection(collNames.getSentence());						
			
			//返回欄位
			BasicDBObject fields = new BasicDBObject();			
			fields.put("content", 1);
			fields.put("context_tw", 1);
			fields.put("context_cn", 1);
			fields.put("sound", 1);
			fields.put("icon", 1);
			fields.put("category", 1);
			fields.put("modifiedDate",1);	
			
			BasicDBObject dbFind = (find == null || find.equals("")) 
					? new BasicDBObject() : (BasicDBObject)JSON.parse(find);  
			dbFind.put("enable", enable);					
			
			BasicDBObject dbSort = (sort == null || sort.equals("")) 
					? new BasicDBObject("modifiedDate",1) : (BasicDBObject)JSON.parse(sort);
			
			
			if(skip > 0){
				cursor = coll.find(dbFind, fields).skip(skip).limit(count).sort(dbSort); //-1 降冪   1升冪
			}else{
				cursor = coll.find(dbFind, fields).limit(count).sort(dbSort); //-1 降冪   1升冪
			}
			
			if(cursor.size() > 0){
				sentenceList = new ArrayList<Map>();
				while(cursor.hasNext()) {					
					Map item = ((BasicDBObject)cursor.next()).toMap();
					String id = item.get("_id").toString();
					item.remove("_id");
					item.put("id", id);					
					sentenceList.add( item );
				}	
			}			
		} catch (UnknownHostException e) {
			logger.error(e.toString());			
		}finally{
			if(cursor != null){
				cursor.close();				
			}
			db = null;
			coll = null;
			cursor = null;
		}		
		return sentenceList;
	}

	@Override
	public boolean insertUser(Map<String, Object> data) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean updateUser(Map<String, Object> data) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean closeUser(String id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<Map> findUser(Map<String, Object> data) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getUserTotal() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public List<Map> pagingUser(String indexId, String count) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Map<String, String>> getRoleList() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Map<String, String>> findRoleList(Map<String, String> data) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean updateRole(Map<String, String> data) {
		// TODO Auto-generated method stub
		return false;
	}

}
