package back.dao;

import java.util.List;
import java.util.Map;

public interface IBackDAO {	
	
	
	/** 取得句詞分類
	 * @param lang
	 * @param enable
	 * @return
	 */
	List<Map> getSentenceCategory(String lang, boolean enable);	

	/**
	 * 新增一筆句子
	 * @param param 
	 * @return
	 */
	boolean insertSentence(Map<String, Object> param);
	
	
	/**
	 * 更新一筆句子
	 * @param id
	 * @param data
	 * @return
	 */
	boolean updateSentence(String id , Map<String,Object> data);
	
	
	/** 下架一筆詞句
	 * @param id
	 * @return
	 */
	boolean offSentence(String id);	
	
	//搜尋 句子
	Map findSentenceList(String find, String lang, boolean enable);	
	

	/**
	 * @param find		搜尋條件  (JSON字串)
	 * @param category	類型  0不分類
	 * @param enable	上下架  0不分 1上架  2 下架
	 * @return
	 */
	public long getSentenceQuantity(String find, String category, int enable) ;
	
	
	/** 分頁功能取句子資料
	 * @param skip		跳躍數
	 * @param count		資料筆數
	 * @param find		搜尋條件  (JSON字串)
	 * @param sort		排序條件  (JSON字串)
	 * @param lang  	語系
	 * @param enable 	上下架
	 * @return List<Map>
	 */
	List<Map> pagingSentence(int skip, int count, String  find, String  sort, boolean enable);	
	
	//加入一個使用者
	boolean insertUser(Map<String,Object> param);
	
	//修改使用者
	boolean updateUser(Map<String,Object> param);
	
	//關閉使用者
	boolean closeUser(String id);
	
	//搜尋使用者
	List<Map> findUser(Map<String,Object> data);
	
	//總數
	long getUserTotal();
	
	//取得使用者 分頁
	List<Map> pagingUser(String indexId, String count);	
	
	
	//加入一個角色
	//關閉一個角色
	//修改一個角色
	//搜尋角色
	//角色總數
	//角色分頁
	
	//加入一筆資源  記得要有一欄seed,作亂數取得用   
	//關閉一筆資源
	//修改一筆資源
	//搜尋資源
	//資源總數
	//取得資源分頁
		
	
	/**
	 * @param sessionID
	 * @return 角色列表
	 */
	List<Map<String,String>> getRoleList();
	
	/**
	 * @param sessionID
	 * @return 查詢角色列表
	 */
	List<Map<String,String>> findRoleList(Map<String,String> data);	
	
	//修改一個角色資料
	boolean updateRole(Map<String,String> data);
	

}
