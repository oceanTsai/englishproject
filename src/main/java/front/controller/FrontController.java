package front.controller;

import global.controller.ControllerBase;
import javassist.bytecode.stackmap.TypeData.ClassName;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/front")
public class FrontController  extends ControllerBase{
	private static final long serialVersionUID = 1L;
	
	private static Logger logger = Logger.getLogger(ClassName.class);	
	
    /**
     * 開啟閱讀頁
     * @param model
     * @return ModelAndView
     */
    @RequestMapping(value= "/read", method=RequestMethod.GET)
    public ModelAndView dispatcherReadPage(HttpSession session){
    	logger.info("dispatcher front.englishRead");
    	
    	ModelAndView view =  new ModelAndView("front.Index");    	
    	boolean hasPriority = super.resourceExist(session, "front/read");
    	if(hasPriority){
    		view.setViewName("front.englishRead"); 
    	}    	   	
    	return view;
    }
    
    /**
     * 開啟狀態頁 
     * @param model
     * @return ModelAndView
     */
    @RequestMapping(value= "/status", method=RequestMethod.GET)
    public ModelAndView dispatcherStatusPage(HttpSession session){
    	logger.info("dispatcher front.englishStatus");
    	
    	ModelAndView view =  new ModelAndView("front.Index");    	
    	boolean hasPriority = super.resourceExist(session, "front/status");
    	if(hasPriority) { 
    		view.setViewName("front.englishStatus" );	
    	}    	
    	return view;
    }    
}
