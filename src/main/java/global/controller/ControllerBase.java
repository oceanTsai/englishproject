package global.controller;

import global.db.DefaultProperty;
import global.memcach.Cach;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import com.whalin.MemCached.MemCachedClient;

@Configuration
public class ControllerBase {    
	//系統屬性
	@Autowired
	protected DefaultProperty defaultProperty;	
	
	//處理memcached 存取
	@Autowired
	private Cach cach;	
	
	//沒有使用權限的訊息
	@Value("${message.no.permissions}")
	private String noPermissions;
	
	public String getNoPermissions(){
		return this.noPermissions;
	}
	
	/**
	 * 檢查是否有權限使用資源
	 * @param session	用來取sessionId
	 * @param url		被檢查的資源位置
	 * @return			有權限回傳 true ,沒權限回傳 false
	 */
	protected boolean resourceExist(HttpSession session, String url){
		String sessionID = session.getId();		
		MemCachedClient mcc = cach.getCacheClient();		
		Map cachResource = (HashMap)mcc.get(sessionID);
		
		//可用的頁面資源
		Object viewData = cachResource.get(defaultProperty.getViewResKey());
		if(viewData != null){
    		List<Map> viewResource = (List<Map>)viewData;
    		for(Map item : viewResource){
    			if( item.get("url").toString().equals(url) ){
    				return true;
    			}
    		}

		}
		
		/*直接存在session的處理方式
		Object data = session.getAttribute(defaultProperty.getViewResKey());
    	if(data != null){
    		List<Map> resource = (List<Map>)data;
    		for( Map item : resource){
    			if(item.get("url").toString().equals(url)){
    				return true;
    			}
    		}
    	}
    	*/
    	return false;
	}
}
