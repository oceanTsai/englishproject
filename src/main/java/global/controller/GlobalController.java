package global.controller;

import global.dao.IGlobalDAO;
import javassist.bytecode.stackmap.TypeData.ClassName;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class GlobalController extends ControllerBase {
	
	private static Logger logger = Logger.getLogger(ClassName.class);
    
	@Autowired
	private IGlobalDAO globalDAO;

    /**
     * 顯示首頁
     * @param model
     * @return ModelAndView
     */
    @RequestMapping(value= {"/","/home"}  ,method = RequestMethod.GET)
    public ModelAndView  dispatcherDefaultPage(HttpSession session ){
    	logger.info("dispatcher home");
    	ModelAndView view = new ModelAndView("front.Index");
    	return view;
    }
}
