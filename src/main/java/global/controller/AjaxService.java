package global.controller;

import global.dao.IGlobalDAO;
import global.db.DefaultProperty;
import global.mappin.Result;
import global.memcach.Cach;
import global.utils.SentenceSpilte;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javassist.bytecode.stackmap.TypeData.ClassName;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import back.dao.IBackDAO;

import com.whalin.MemCached.MemCachedClient;

@Service
@RequestMapping("/service")
public class AjaxService {
	
	private static Logger logger = Logger.getLogger(ClassName.class);
	
	//系統屬性
	@Autowired
	protected DefaultProperty defaultProperty;
	
	//全域DAO物件
	@Autowired
	private IGlobalDAO globalDAO;
	
	//後台DAO物件
	@Autowired
	private IBackDAO backDAO;
	
	//memcached 存取
	@Autowired
	private Cach cach;
	
	//字串切割器
	@Autowired
	private SentenceSpilte spilte;
	
	//登入失敗錯誤訊息
	@Value("${message.login.fail}")
	private String loginFailmsg;	

	
	/** 取角色可用資原列表
	 * @param session
	 * @return
	 */
	@RequestMapping(value="/resource" , method = RequestMethod.GET)
	public Result getResourceList(HttpSession session ){
		Result result = new Result();
		
		MemCachedClient mcc = cach.getCacheClient();
		String sessionID = session.getId();	
		Object oresource = mcc.get(sessionID);
		
		if(oresource != null){
			Map<String, Object> cachResource = (HashMap<String, Object>)oresource;
			
			Object resource = cachResource.get( defaultProperty.getViewResKey() );		
	    	if(resource != null){    
	    		result.resource =  (List<Map>)resource; 
	    	} 
	    	
	    	Object user = cachResource.get( defaultProperty.getsUserKey() );
	    	if(user != null){
	    		result.user = (Map)user;
	    	}
	    	
			Object role = cachResource.get( defaultProperty.getRoleKey() );
			if(role != null){
				result.role = (Map)user;
			}
		}
		/* 直接存session的處理方式
		Object resource = session.getAttribute(defaultProperty.getViewResKey());		
    	if(resource != null){    
    		result.resource =  (List<Map>)resource; 
    	} 
    	
    	Object user = session.getAttribute(defaultProperty.getsUserKey());
    	if(user != null){
    		result.user = (Map)user;
    	}
    	
		Object role = session.getAttribute(defaultProperty.getRoleKey());
		if(role != null){
			result.role = (Map)user;
		}   	
    	*/
    	return result;		
	}
	
	/**
	 * (登入時)
	 * @param mail
	 * @param pwd
	 * @return Permissions
	 */
	@RequestMapping(value="/login" , method = RequestMethod.POST)
	public Result singIn(@RequestParam(value = "mail")String mail, @RequestParam(value = "pwd")String pwd, HttpSession session) {		
		Result result = new Result();		
		
		//auth user
		Map user = globalDAO.authUser(mail, pwd, true);		
		
		//not null 表示  驗證通過
		if(user != null){
			Map cachResource = null;
			
			MemCachedClient mcc = cach.getCacheClient();	
			String sessionID = session.getId();					
			Object data = mcc.get(sessionID);
			
			cachResource = (data == null) ? new HashMap() : (HashMap)data ;
			
			//user
			cachResource.remove( defaultProperty.getsUserKey() );
			cachResource.put( defaultProperty.getsUserKey(), user );			
			result.user = user;
			
			//role
			String roleID = user.get("roleID").toString();
			Map role = globalDAO.getRole(roleID, true);
			cachResource.remove( defaultProperty.getRoleKey() );
			cachResource.put( defaultProperty.getRoleKey(), role );
			result.role = role;			
			
			//result
			Integer priority = (Integer)role.get("priority");
			ArrayList layer = (ArrayList)role.get("layer");
			ArrayList type = new ArrayList();
			type.add("2");		
			List<Map> res = globalDAO.getResourceList(priority, layer, type, true);
			cachResource.remove( defaultProperty.getViewResKey() );
			cachResource.put( defaultProperty.getViewResKey(), res );			
			result.resource = res;		
			
			//result
			result.success = true;
			logger.info("account auth pass");
			mcc.set(sessionID, cachResource);
			
			/* 直接存session的方式
			session.setAttribute(defaultProperty.getsUserKey(), user);
			result.user = user;
			
			//role
			String roleID = user.get("roleID").toString();
			Map role = globalDAO.getRole(roleID, true);
			session.setAttribute(defaultProperty.getRoleKey(), role);
			result.role = role;			
			
			//result
			Integer priority = (Integer)role.get("priority");
			ArrayList layer = (ArrayList)role.get("layer");
			ArrayList type = new ArrayList();
			type.add("2");		
			List<Map> res = globalDAO.getResourceList(priority, layer, type, true);
			session.setAttribute(defaultProperty.getViewResKey(), res);
			result.resource = res;		
			result.success = true;
			logger.info("account auth pass");
			*/
		}else{
			result.message = loginFailmsg;
			result.success = false;
			logger.info("account auth not pass");
		}		
		return result;
	}
	
	//登出
	@RequestMapping(value="/logout" , method = RequestMethod.POST)
	public Result singOut(HttpSession session) {		
		MemCachedClient mcc = cach.getCacheClient();
		String sessionID = session.getId();
		Map cachResource = (HashMap)mcc.get(sessionID) ;		
		
		cachResource.remove( defaultProperty.getsUserKey() );	//clear user		
		cachResource.remove( defaultProperty.getRoleKey() );	//clear role		
		cachResource.remove( defaultProperty.getViewResKey() ); 
		mcc.set(sessionID, cachResource);
		logger.info("clear sission");
		
		//clear session	data
		/*
		session.removeAttribute(defaultProperty.getsUserKey());	//clear user		
		session.removeAttribute(defaultProperty.getRoleKey());	//clear role		
		session.removeAttribute(defaultProperty.getViewResKey());		
		logger.info("clear sission");
		*/		
		return new Result();
	}

	//隨機取一筆句子
	@RequestMapping(value="/randomFind" , method = RequestMethod.GET)
	public Result randomFindSentence(@RequestParam(value = "sessionID")String sessionID){
		Result result = new Result();		
		Map sentenceHits = null;
		//取資料
		Map data = globalDAO.getRandomSentence(true);
		//分割字串
		ArrayList<String> englistList = null;
		ArrayList<String> symbolList = null;
		if(data != null){
			String content = data.get("content").toString();
			englistList = spilte.getEnglist(content);
			symbolList = spilte.getSymbol(content);
			data.put("englistList", englistList);
			data.put("symbolList", symbolList);			
		}		
		//result
		result.data = data;		
		result.success = true;
		
		//cache
		String sentenceID = data.get("id").toString();		
		MemCachedClient mcc = cach.getCacheClient();		
		Map cachResource = (HashMap)mcc.get(sessionID) ;	
		//紀錄目前的文句ID
		cachResource.remove("nowSentenceID");
		cachResource.put("nowSentenceID", sentenceID);
		
		//詞沒有在memcached裡要將結果存進去
		Object item = cachResource.get(sentenceID);
		if(item == null){
			sentenceHits = new HashMap();
			for(int i=0 ; i < englistList.size() ; i++){
				String word = englistList.get(i);
				sentenceHits.put(word, new int[]{0,i}); //第一次取到的資料的點擊數是零  [點擊數,排列順序]
			}
			cachResource.put(sentenceID, sentenceHits);			
		}
		mcc.set(sessionID, cachResource);
		return result;
	}
	
	//加點擊數量
	@RequestMapping(value="/increaseHits" , method = RequestMethod.POST)
	public Result increaseHits(
			@RequestParam(value = "sessionID")String sessionID,
			@RequestParam(value = "sentenceID")String sentenceID,
			@RequestParam(value = "word")String word){
		Result result = new Result();		
		Map sentenceHits = null;
		
		MemCachedClient mcc = cach.getCacheClient();		
		Map cachResource = (HashMap)mcc.get(sessionID);		
		Object item = cachResource.get(sentenceID);
		if(item != null){
			sentenceHits = (HashMap)item;
			int [] wordItem = (int[])sentenceHits.get(word);
			wordItem[0]++; //hit ++			
			sentenceHits.remove(word);
			sentenceHits.put(word, wordItem);
			cachResource.remove(sentenceID);
			cachResource.put(sentenceID, sentenceHits);			
			mcc.set(sessionID, cachResource);
			result.success = true;
			result.data = sentenceHits;
		}	
		return result;
	}
	
	//清空點擊
	@RequestMapping(value="/clearHits" , method = RequestMethod.POST)
	public Result clearHits(
			@RequestParam(value = "sessionID")String sessionID,
			@RequestParam(value = "word")String word){			
		Result result = new Result();
		Map sentenceHits = null;
		
		MemCachedClient mcc = cach.getCacheClient();		
		Map cachResource = (HashMap)mcc.get(sessionID);
		String sentenceID = cachResource.get("nowSentenceID").toString();		
		
		Object item = cachResource.get(sentenceID);
		
		if(item != null){
			sentenceHits = (HashMap)item;
			int [] wordItem = (int[])sentenceHits.get(word);
			wordItem[0] = 0; //clear			
			sentenceHits.remove(word);
			sentenceHits.put(word, wordItem);
			cachResource.remove(sentenceID);
			cachResource.put(sentenceID, sentenceHits);			
			mcc.set(sessionID, cachResource);
			result.success = true;
			result.data = sentenceHits;
		}		
		return result;		
	}
	
	//取得單字點擊列表
	@RequestMapping(value="/getWordList" , method = RequestMethod.GET)
	public Result getWordList(@RequestParam(value = "sessionID")String sessionID){
		Result result = new Result();
		Map sentenceHits = null;
		
		MemCachedClient mcc = cach.getCacheClient();		
		Map cachResource = (HashMap)mcc.get(sessionID);
		Object id = cachResource.get("nowSentenceID");
		if(id != null){
			String sentenceID = id.toString();
			sentenceHits = (HashMap)cachResource.get(sentenceID);
			result.success = true;
			result.data = sentenceHits;
		}		
		return result;
	};
	

	
	
}
