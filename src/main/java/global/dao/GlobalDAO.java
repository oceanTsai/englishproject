package global.dao;

import global.db.CollectionNames;
import global.db.DefaultProperty;
import global.db.MongoConnection;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javassist.bytecode.stackmap.TypeData.ClassName;

import org.apache.log4j.Logger;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;


/**
 * @author hua
 * 資料查詢物件
 */
@Configuration
@Scope(BeanDefinition.SCOPE_SINGLETON)
public class GlobalDAO implements IGlobalDAO {

	private static Logger logger = Logger.getLogger(ClassName.class);
	
	//系統屬性
	@Autowired
	private DefaultProperty defaultProperty;	
	
	//資料表名稱
	@Autowired
	private CollectionNames collNames;
	
	//資料庫連線物件
	@Autowired	
	private MongoConnection mongoConnection;	

	
	@Override
	public Map authUser(String mail, String pwd, boolean enable) {
		DB db = null;
		DBCollection coll = null;
		Map user = null;
		
		try {					
			db = mongoConnection.getDB();
			coll = db.getCollection(collNames.getUser());
			//查詢條件
			BasicDBObject query = new BasicDBObject();
			query.put("mail", mail);	//account
			query.put("password", pwd);
			query.put("enable", enable);
			
			//返回欄位
			BasicDBObject fields = new BasicDBObject();
			fields.put("_id", 1);// 1 顯示   0 剔除
			fields.put("name", 1);
			fields.put("cname", 1);
			fields.put("roleID",1);
			
			DBObject result = coll.findOne(query, fields);			
			if(result == null){
				logger.info("not find " + mail);
			}else{				
				user = result.toMap();
				String id = user.get("_id").toString();	//取回存String ID值
				user.remove("_id");	//移除ObjectId 
				user.put("id", id);				
			}
		} catch (UnknownHostException e) {
			logger.error(e.toString());			
		}finally{
			db = null;
			coll = null;
		}		
		return user;		
	}

	
	@Override
	public Map<String, String> getRole(String roleID, boolean enable) {
		DB db = null;
		DBCollection coll = null;
		Map role = null;
		
		try {					
			db = mongoConnection.getDB();
			coll = db.getCollection(collNames.getRole());			
			//查詢條件
			BasicDBObject query = new BasicDBObject();
			query.put("_id", new ObjectId(roleID));
			query.put("enable", enable);
			
			//返回欄位
			BasicDBObject fields = new BasicDBObject();
			fields.put("_id", 1);// 1 顯示   0 剔除			
			fields.put("name_" + defaultProperty.getLang() , 1);			
			fields.put("priority",1);	//角色權重
			fields.put("layer", 1);		//角色可以使用的平台
			
			DBObject result = coll.findOne(query, fields);			
			if(result == null){
				logger.info("not find " + roleID);
			}else{
				role = result.toMap();
				String id = role.get("_id").toString();
				role.remove("_id");	
				role.put("id", id);				
			}
		} catch (UnknownHostException e) {
			logger.error(e.toString());			
		}finally{			
			db = null;
			coll = null;			
		}		
		return role;	
	}
	
	
	@Override
	public List<Map> getResourceList(Integer priority, ArrayList layer , ArrayList type, boolean enable){
		
		DB db = null;
		DBCursor cursor  = null;
		List<Map> resource = null;
		
		try {					
			db = mongoConnection.getDB();
			DBCollection coll = db.getCollection(collNames.getResource());						
			
			BasicDBObject query = new BasicDBObject();  //enable=true AND priority <= ? AND layer in (?) AND type in (?)
			query.put("enable", enable);
			query.put("priority", new BasicDBObject("$lte",priority));	//小於等於的用法
			query.put("layer", new BasicDBObject("$in", layer));		//in的用法
			query.put("type", new BasicDBObject("$in", type));			
			
			BasicDBObject fields = new BasicDBObject();
			fields.put("url", 1);
			fields.put("parent", 1);
			fields.put("name_en", 1);
			fields.put("name_" + defaultProperty.getLang(), 1);
			fields.put("glyphicon", 1);			
			fields.put("description", 1);
			fields.put("order", 1);	//排序要用到			
			
			cursor = coll.find(query, fields).sort(new BasicDBObject("order",1)); //-1 降冪   1升冪		
			if(cursor.size() > 0){
				resource = new ArrayList<Map>();
				while(cursor.hasNext()) {
					Map item = ((BasicDBObject)cursor.next()).toMap();
					String id = item.get("_id").toString();
					item.remove("_id");	
					item.put("id", id);					
					resource.add( item );
				}	
			}		
		} catch (UnknownHostException e) {
			logger.error(e.toString());			
		}finally{
			if(cursor != null){
				cursor.close();				
			}
			db = null;
			cursor = null;
		}		
		return resource;
	}	
	

	//隨機取得一筆詞句，使用地理空間隨機取得
	@Override
	public Map getRandomSentence(boolean enable){
		DB db = null;
		DBCollection coll = null;
		Map sentence = null;
		try {					
			db = mongoConnection.getDB();
			coll = db.getCollection(collNames.getSentence());
			
			Random random = new Random();
			double ranNum = random.nextDouble();
			
			
			//返回欄位
			BasicDBObject fields = new BasicDBObject();
			fields.put("_id", 1);// 1 顯示   0 剔除
			fields.put("content", 1);
			fields.put("context_tw", 1);
			fields.put("context_cn", 1);
			fields.put("sound", 1);
			fields.put("icon", 1);
			fields.put("category", 1);
			fields.put("random", 1);
			
			//查詢條件
			ArrayList matrix = new ArrayList();
			matrix.add(ranNum);
			matrix.add(0);
			BasicDBObject query = new BasicDBObject();			
			query.put("enable", true);
			query.put("random", new BasicDBObject("$near", matrix));			
			//find
			DBObject result = coll.findOne(query, fields);			
							
			sentence = result.toMap();
			String id = sentence.get("_id").toString();			
			sentence.remove("_id");
			sentence.put("id", id);
				
		} catch (UnknownHostException e) {
			logger.error(e.toString());			
		}finally{
			db = null;
			coll = null;
		}		
		return sentence;	
	}
	/* 這是map reduce 方式，考慮過後改掉
	 * 
	@Autowired
	private MapReduceCmdGenerator cmdGenerator; //取 map reduce function
	
	@Override
	public Map<String, String> authUser(String mail, String pwd , boolean enable) {		
		
		DB db = null;
		Map<String, String> user = null;
		MapReduceCommand cmd = null;
		MapReduceOutput mapreduceOut = null;
		
		try{			
			db = mongoConnection.getDB();				
			DBCollection userColl = db.getCollection(collNames.getUser());			
			
			//get user mpa and reduce
			String userMap = cmdGenerator.getUserMap(mail, pwd, enable);
			String reducer = cmdGenerator.getCollectionReduce();		   
			//excute user mapReduce			
		    cmd = new MapReduceCommand(userColl, userMap, reducer, "userRoleView", 
		    							MapReduceCommand.OutputType.REDUCE, null);			
		    mapreduceOut = userColl.mapReduce(cmd);
		    
		    //get roleID by user map result
		    String roleID = "";
			for (DBObject item : mapreduceOut.results()) {
				roleID = item.get("_id").toString();					
			}		    
			user = new HashMap<String, String>();				
			
		}catch(UnknownHostException e){
			logger.error(e.toString());
		}finally{	
			db=null;
		}		
		
		return user;
	}	
	
	public Map<String,String> getRole(String roleID){		
		DB db = null;
		Map<String, String> role = null;
		MapReduceCommand cmd = null;
		MapReduceOutput out = null;
		
		try{
			db = mongoConnection.getDB();
			DBCollection collRole = db.getCollection(collNames.getRole());		
			
			//get role mpa and reduce
			String roleMap = cmdGenerator.getRoleMap(roleID);
			String reducer = cmdGenerator.getCollectionReduce();
			
			cmd = new MapReduceCommand(collRole, roleMap, reducer, "userRoleView",
										MapReduceCommand.OutputType.REDUCE, null);
			
			out = collRole.mapReduce(cmd);
			role = new HashMap<String, String>();
		
			for (DBObject item : out.results()) {
			       System.out.println(item.toString());
			}		
			
		}catch(UnknownHostException e){
			
		}finally{
			db.getCollection("userRoleView").remove( new BasicDBObject());//remove all data
			db=null;
		}			
		return role;
	}*/

}
