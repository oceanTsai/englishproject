package global.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public interface IGlobalDAO {
	
	/**
	 * 驗證使用者<br/>
	 * 		● 通過：後取回使用者資料。<br/>
	 * 		● 失敗：回傳null。<br/>
	 * @param mail		
	 * @param pwd		
	 * @param enable	在資料庫中是否設定為啟用
	 * @return
	 */
	Map authUser(String mail, String pwd, boolean enable);
	
	/**
	 * 取回角色資料
	 * @param roleID
	 * @return
	 */
	Map getRole(String roleID, boolean enable);
	
	
	/** 取得導覽列資料
	 * @param priority  權限
	 * @param layer     1. 全域   2. 前台  3後台
	 * @param type      1. node 2 page  3 service 4 image 5 video  6 audio
	 * @param enable	上下架的狀態
	 * @return
	 */
	List<Map> getResourceList(Integer priority, ArrayList layer , ArrayList type, boolean enable);
		
	/**
	 * @param skip
	 * @return
	 */
	Map getRandomSentence(boolean enable);

}