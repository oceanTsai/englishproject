package global.db;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

/**
 * @author hua
 * 用來取得系統預設數值
 */
@Configuration
@Scope(BeanDefinition.SCOPE_SINGLETON)
public class DefaultProperty {
	
	@Value("${default.role}")
	private String defaultRole;
	
	@Value("${session.key.user}")
	private String sessionUserKey;
	
	@Value("${session.key.role}")
	private String sessionRoleKey;
	
	//頁面資源 key
	@Value("${session.key.resource}")
	private String sessionResourceKey;	
	
	//ajax service 資源 key
	@Value("${session.key.ajax.resource}")
	private String sessionAjaxResourceKey;
	
	//語系
	@Value("${sys.lang}")
	private String lang;
	

	
	/**
	 * 未登入時預設的角色 (匿名)
	 * @return
	 */
	public String getDefaultRoleID() {
		return this.defaultRole;
	}

	/**
	 * session中存放使用者資料的key
	 * @return
	 */
	public String getsUserKey() {
		return this.sessionUserKey;
	}

	/**
	 * session中存放角色資料的key
	 * @return
	 */
	public String getRoleKey() {
		return this.sessionRoleKey;
	}

	/**
	 * session中存放可用資源(頁面)的key
	 * @return
	 */
	public String getViewResKey() {
		return this.sessionResourceKey;
	}
	
	/**session中存放可用資源(ajax service)的key
	 * @return
	 */
	public String getAjaxResKey(){
		return this.sessionAjaxResourceKey;
	}
	
	/**
	 * 系統語系
	 * @return
	 */
	public String getLang(){
		return this.lang;
	}
	
}
