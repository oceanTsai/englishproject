package global.db;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

/**
 * @author hua <br/>
 * 用來取得Collection 名稱。
 */
@Configuration
@Scope(BeanDefinition.SCOPE_SINGLETON)
public class CollectionNames {
	
	@Value("${mongo.coll.user}")
	private String user;
	
	@Value("${mongo.coll.role}")
	private String role;
	
	@Value("${mongo.coll.resource}")
	private String resource;
	
	@Value("${mongo.coll.sentence}")
	private String sentence;
	
	@Value("${mongo.coll.sentenceCategory}")
	private String sentenceCategory;
	
	public String getUser(){
		return this.user;
	}
	
	public String getRole(){
		return this.role;
	}	

	public String getResource(){
		return this.resource;
	}
	
	/**
	 * 詞庫
	 * @return
	 */
	public String getSentence(){
		return this.sentence;
	}
	
	/**
	 * 詞句分類
	 * @return
	 */
	public String getSentenceCategory(){
		return this.sentenceCategory;
	}
}
