package global.db;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

/**
 * @author hua
 * map 與  reduce 方法字串產生器。
 */
@Configuration
@Scope(BeanDefinition.SCOPE_SINGLETON)
public class MapReduceCmdGenerator {
	
	/**
	 * 取得使用者資料
	 * @return
	 */
	public String getUserMap(String mail, String pwd, boolean enable){
		
		StringBuffer map = new StringBuffer();
		map.append( "function () {" );	
		map.append( "				if( this.mail === '" ).append(mail).append("' &&");
		map.append( "					this.password === '" ).append(pwd).append("' &&");
		map.append( "					this.enable == " ).append(enable).append(" )");
		map.append("				{ ");
		map.append( "					var values = { name : this.name, cname : this.cname , account : this.mail}; " );	//輸出的欄位
		map.append( "					emit( this.roleID , values );"	);
		map.append("				}");
		map.append( "}" );
		return map.toString();
	}	
	
	public String getRoleMap(String roleId){		
		StringBuffer map = new StringBuffer();
		map.append( "function () {" );	
		map.append(	"				if( this._id == '" + roleId  + "' ){" );
		map.append( "					var values = { roleName_en : this.name_en, roleName_tw : this.name_tw," );	//輸出的欄位
		map.append( "								   roleName_cn : this.name_cn, priority : this.priority, layer : this.layer};");
		map.append( "					emit( this._id , values );"	);
		map.append(	"				}" );
		map.append( "}" );
		return map.toString();
	}
	
	/**
	 * collection 格式輸出
	 * @return
	 */
	public String getCollectionReduce(){
	    StringBuffer reduce = new StringBuffer();
	    reduce.append( "function(key, values){");
	    reduce.append( "		    	 var result = {} ;");
	    reduce.append( "		    	 values.forEach( function(value){" );
	    reduce.append( "		    	  var field;");
	    reduce.append( "		    	  for(field in value)");
	    reduce.append( "		    	   if( value.hasOwnProperty(field) )");
	    reduce.append( "		    	    result[field] = value[field];");
	    reduce.append( "		    	 });");
	    reduce.append( "		    	 return result;");
	    reduce.append( "		    	}");
	    return reduce.toString();
	} 

}
