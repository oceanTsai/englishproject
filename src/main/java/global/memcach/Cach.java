package global.memcach;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import com.whalin.MemCached.MemCachedClient;
import com.whalin.MemCached.SockIOPool;


/**
 * @author hua
 *	存取memcached連線工具
 */
@Configuration
@Scope(BeanDefinition.SCOPE_SINGLETON)
public class Cach {
	
	@Value("${memcached.pool.host}")
	private String [] host;	
	
	@Value("${memcached.pool.weights}")
    private Integer [] weights;  
    
	@Value("${memcached.pool.initConn}")
	private int initConn;
	
	@Value("${memcached.pool.minConn}")
	private int minConn;
	
	@Value("${memcached.pool.maxConn}")
	private int maxConn;
	
	@Value("${memcached.pool.maxIdle}")
	private int maxIdle;
	
	@Value("${memcached.pool.maintSleep}")
	private int sleep;
	
	@Value("${memcached.pool.nagle}")
	private boolean nagle;
	
	@Value("${memcached.pool.socketTO}")
	private int socketTo;
	
	@Value("${memcached.pool.socketConnectTO}")
	private int socketConnectTO;
	
    private SockIOPool pool;
	
	private MemCachedClient cachedClient;	
	 
	private void buildConnection(){		
		cachedClient =  new MemCachedClient();
		pool = SockIOPool.getInstance(); 			
		pool.setServers(host);  
        pool.setWeights(weights);  
        pool.setInitConn(initConn);  
        pool.setMinConn(minConn);  
        pool.setMaxConn(maxConn);  
        pool.setMaxIdle(maxIdle);  
        pool.setMaintSleep(sleep);  
        pool.setNagle(nagle);  
        pool.setSocketTO(socketTo);  
        pool.setSocketConnectTO(socketConnectTO);        
        pool.initialize();        
	}
	
	public MemCachedClient getCacheClient(){
		synchronized (this) {
			if(cachedClient == null){
				buildConnection();
			}			
		}	
		return cachedClient;
	}
}