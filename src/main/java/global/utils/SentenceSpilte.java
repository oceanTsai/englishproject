package global.utils;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.context.annotation.Configuration;

@Configuration
public class SentenceSpilte {
	
	/**
	 * 取得字串內所有英文單字序列
	 * @param str
	 * @return
	 */
	public ArrayList<String> getEnglist(String str){
		ArrayList<String> list = new ArrayList<String>();
		Pattern pattern = Pattern.compile("[a-z|A-z|0-9|\\']+");
		Matcher matcher = pattern.matcher(str);		
		while(matcher.find()){
			list.add(matcher.group());
		}
		return list;		
	}
	
	/**取得所有符號序列
	 * @param str
	 * @return
	 */
	public ArrayList<String> getSymbol(String str){
		ArrayList<String> list = new ArrayList<String>();
		Pattern pattern = Pattern.compile("[。？！?!,\\.\\s]+");
		Matcher matcher = pattern.matcher(str);		
		while(matcher.find()){
			list.add(matcher.group());
		}
		return list;
	}
}
