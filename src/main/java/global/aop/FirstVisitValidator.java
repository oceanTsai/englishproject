package global.aop;

import global.dao.IGlobalDAO;
import global.db.DefaultProperty;
import global.memcach.Cach;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javassist.bytecode.stackmap.TypeData.ClassName;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import com.whalin.MemCached.MemCachedClient;

/**
 * @author ocean hua
 *	Spring AOP 
 *   用來檢查是否第一次拜訪網站， <br/>
 *   並取回預設角色、可用資源。  <br/>
 */
@Configuration
@Aspect
public class FirstVisitValidator {
	
	//系統屬性
	@Autowired
	private DefaultProperty defaultProperty;
	
	@Autowired
	private IGlobalDAO globalDAO;
	
	//memcached 存取
	@Autowired
	private Cach cach;
	
	private static Logger logger = Logger.getLogger(ClassName.class);
	
	//首頁
	@Before("execution(* global.controller.GlobalController.dispatcherDefaultPage*(..))&& args(session)")
    public void pointcutDefaultPage(HttpSession session) {		
		checkFirstVisit(session);
    }
	
	//狀態
    @Before("execution(* front.controller.FrontController.dispatcherStatusPage*(..)) && args(session)")
    public  void pointcutStatusPage(HttpSession session){
    	checkFirstVisit(session);
    }
    
    //讀取
    @Before("execution(* front.controller.FrontController.dispatcherReadPage*(..)) && args(session)")
    public  void pointcutReadPage(HttpSession session){
    	checkFirstVisit(session);
    }
    
    //管理
    @Before("execution(* back.controller.BackController.dispatcherManagementPage*(..)) && args(session)")
    public  void pointcutManagementPage(HttpSession session){
    	checkFirstVisit(session);
    }
    
    //資源管理
    @Before("execution(* back.controller.BackController.dispatcherResManagementPage*(..)) && args(session)")
    public  void pointcutResManagementPage(HttpSession session){
    	checkFirstVisit(session);
    }
    
    //句子編輯
    @Before("execution(* back.controller.BackController.dispatcherEditorPage*(..)) && args(session)")
    public  void pointcutdEditorPage(HttpSession session){
    	checkFirstVisit(session);
    }
    
    //ajax 登出後 需要重新拿取  預設狀態資料 (匿名使用者權限) ，登出時會清空session
    @After("execution(* global.controller.AjaxService.singOut*(..)) && args(session)")
    public void pointcutdSingOut(HttpSession session){
    	checkFirstVisit(session);
    }
 
	/**
	 * 檢查是否第一次拜訪 <br/>
	 * 第一次拜訪需取回系統預設「角色」、「可用資源」<br/>
	 * 資源： 頁面路徑、 ajax service 路徑
	 * @param session
	 */
	public void checkFirstVisit(HttpSession session){		
		String sessionID = session.getId();
		logger.info("sessionId = " + sessionID);
		
		MemCachedClient mcc = cach.getCacheClient();		
		Object res  = mcc.get(sessionID);		
		
		Map cachResource = (res == null) ? new HashMap() : (HashMap)res ;		
		Object item = cachResource.get( defaultProperty.getViewResKey() );
		
		//沒資料代表第一次使用網站
		if(item == null){			
    		//get default role
    		Map role = getDefaultRole();
    		cachResource.put( defaultProperty.getRoleKey(), role );    		
    		
    		//get default resource
    		ArrayList type = new ArrayList();     	
         	type.add( "2" );	//type 2 是只取頁面類      	
         	cachResource.put( defaultProperty.getViewResKey(), getResource( role, type ) );    		
    		
    		//get ajax resource
    		type.clear();    		
    		type.add("3");
    		type.add("4");
    		List<Map> ajaxResource = getResource(role, type);	
    		cachResource.put( defaultProperty.getAjaxResKey(), ajaxResource );
           
    		//Date date=new Date(2000000);
    		//save resource to memcached
    		mcc.set(sessionID, cachResource);    		
		}
		
		Map nowRole = (HashMap)cachResource.get( defaultProperty.getRoleKey() );
		String id = nowRole.get("id").toString();
		if(id.equals(defaultProperty.getDefaultRoleID())){
			//代表未登入
			session.setAttribute("isLogin", false);
		}else{
			session.setAttribute("isLogin", true);
		}
		
		/* 直接存session的方式
    	Object item = session.getAttribute(defaultProperty.getViewResKey());
    	boolean firstVisit = (item  == null);
    	if(firstVisit){
    		//get default role
    		Map role =  getDefaultRole();
    		session.setAttribute(defaultProperty.getRoleKey(), role);    		    		
    		
    		//get default resource
    		ArrayList type = new ArrayList();     	
         	type.add( "2" );	//type 2 是只取頁面類  
    		List<Map> resource = getResource(role, type);
    		session.setAttribute(this.defaultProperty.getViewResKey(), resource);    		
    		
    		//get ajax resource
    		type.clear();    		
    		type.add("3");
    		type.add("4");
    		List<Map> ajaxResource = getResource(role, type);
    		session.setAttribute(this.defaultProperty.getAjaxResKey(), ajaxResource);    		
    	}*/    	
	} 	
    
    /**
     * 取預設角色
     * @return Map
     */
    private Map getDefaultRole(){
    	String defaultRoleId = defaultProperty.getDefaultRoleID();
    	return globalDAO.getRole(defaultProperty.getDefaultRoleID(), true);    
    }    
      
    
    /**
     * 取得角色可用資源
     * @param role
     * @param type  1 view node 2 view 3 service node 4 service
     * @return List<Map>
     */
    private List<Map> getResource(Map role, ArrayList type){
    	ArrayList layer = (ArrayList)role.get("layer");     	
     	int priority = (int)role.get("priority");     	
    	return globalDAO.getResourceList(20000, layer, type, true);    	
    }

    
}