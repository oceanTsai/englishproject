package global.mappin;

import java.util.List;
import java.util.Map;

/**
 * @author hua
 * 使用者、角色、可用資源映射<br/>
 * 
 * 目的，統一client端接收處理。
 */
public class Result {

	public Map user;
	
	public Map role;
	
	public List<Map> resource;	

	public String redirection;	
	
	public String message;
	
	public String status;
	
	public boolean success;

	public Object data;
	
}
