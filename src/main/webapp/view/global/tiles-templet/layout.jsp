<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<spring:eval var="bootstrap" expression="@commonProperties.nginx_bootstrap"/>
<spring:eval var="js" expression="@commonProperties.nginx_js"/>
<spring:eval var="plugin" expression="@commonProperties.nginx_plugin"/>
<spring:eval var="image" expression="@commonProperties.nginx_image"/>
<!DOCTYPE html>
<html>
<head>
	<title><tiles:insertAttribute name="title" ignore="true" /></title>	
	<tiles:insertAttribute name="head" />
	<!-- Bootstrap -->
	<link rel="stylesheet" href="${bootstrap}/css/bootstrap.min.css" >	
	<link rel="stylesheet" href="${bootstrap}/css/bootstrap-theme.min.css">

	<!-- loading 效果 -->
	<link rel="stylesheet" href="${plugin}/Ladda/dist/ladda-themeless.min.css" >	
	<!-- auto complete -->
	<link rel="stylesheet" href="${plugin}/jquery-ui-Autocomplete/jquery-ui.min.css" >		

	<!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
	<!--[if lt IE 9]>
	<script src="${plugin}/ie8-responsive-file-warning.js"></script>
	<script src="${plugin}/JSON-js/json2.js"></script>
	<![endif]-->
	<script src="${plugin}/ie-emulation-modes-warning.js"></script>

	<!-- 
		html5shiv 讓IE9以下的IE支援 HTML5標籤
		respond   讓IE9以下的IE支援 Media Query -->
	<!--[if lt IE 9]>
	  <script src="${plugin}/html5shiv/dist/html5shiv.min.js"></script>
	  <script src="${plugin}/Respond/dest/respond.min.js"></script>
	<![endif]-->
	<script src="${plugin}/jquery-1.11.1.min.js"></script>
	<script src="${plugin}/knockout-3.1.0.js"></script> 	<!-- MVVM -->		
	<script src="${plugin}/jquery.transit.min.js"></script> <!-- 效果 -->	
	<script src="${plugin}/Ladda/dist/spin.min.js"></script> <!-- 按鈕上 loading 效果 -->
	<script src="${plugin}/Ladda/dist/ladda.min.js"></script> <!-- 按鈕上 loading 效果 -->
	<script src="${js}/service.js"></script>
	<script src="${plugin}/blockui/jquery.blockUI.js"></script> <!-- loading mask -->
	<script src="${js}/blockModel.js"></script><!-- loading mask 操作抽出 -->
	<script src="${plugin}/jquery-cookie/src/jquery.cookie.js"></script>
	
	<style>
	/**解決bootstrap nav bar fix top bug*/
	 body {
    	padding-top: 70px;
  	}
	</style>
</head>

<body role="document">
	<tiles:insertAttribute name="header" />
	<tiles:insertAttribute name="content" />	
	<tiles:insertAttribute name="footer" />	
	<script>
		if(!window.pageModel){
			window.pageModel = {};								
		}
	</script>	
	<script src="${bootstrap}/js/bootstrap.min.js"></script>	
	<script src="${plugin}/jquery-ui-Autocomplete/jquery-ui.min.js"></script>	
	<script src="${js}/menuModel.js"></script>
	<script src="${js}/loginModel.js"></script>	
	<script>		
		(function(){
			//模組控制器
			var modelController = {
					service : null,		
					menuModel : null,
					loginModel : null,
					//加入偵聽
					addEventListener : function(){	
						//驗證成功
						var eventName = this.loginModel.EVENT_LOGIN_SUCCESS;						
						this.loginModel.addSubject( eventName , this.loginSuccess );
						
						//驗證不通過
						eventName = this.loginModel.EVENT_LOGIN_FAIL;						
						this.loginModel.addSubject( eventName , this.loginFail );
						
						//登出成功
						eventName = this.loginModel.EVENT_LOGOUT_SUCCESS;
						this.loginModel.addSubject( eventName , this.logOutSuccess );											
					},
					loginSuccess : function(){
						//var that = modelController; 
						//that.prepareResource();
						//that = null;						
						location.reload();
					},	
					loginFail : function(){
						alert('登入失敗!，請確定帳號、密碼輸入正確。');
					},
					logOutSuccess : function(){
						//var that = modelController; 
						//that.prepareResource();
						//that = null;
						location.href = "/englishProject/home";
						//location.reload();
						
					},
					//給各個Model設定資料，knockoutjs會依資料驅動view。
					setResourceForModel : function(result){						
						window.pageModel.menuModel.setResource(result.resource);
						
						var isLogIn = (result.user != null);	
						window.pageModel.loginModel.setIsLogin(isLogIn);
						//
						if(window.pageModel.pageCore){
							window.pageModel.pageCore.excuted();
						}
					
					},	
					//取回資源成功 (user、role、resource)
					getResourceSuccess : function( data, textStatus, jqXHR ){
						if( data!=null && data.result!=null ){
							//save data
							window.pageModel.resource = data.result.resource;
							window.pageModel.user = data.result.user;
							window.pageModel.role =  data.result.role;							
							//
							this.target.setResourceForModel( data.result );														
						}
					},
					getResourceFail : function( jqXHR, textStatus, errorThrown ){						
					},			
					//準備環境所需資源
					prepareResource : function(){						
						this.service.getResource( this.getResourceSuccess, this.getResourceFail, this);						
					},
					//將cookie資料倒出給autocomplete
					initEmailAutocomplete : function(){						
						 var catchValue = $.cookie('account');						 
						 if(catchValue){
							 var accounts = catchValue.split(',');
							 this.loginModel.accountView.autocomplete({source: accounts });
						 }						 
					},					
					init : function(service, menuModel, loginModel){
						this.service = service;
						this.menuModel = menuModel;
						this.loginModel = loginModel;						
						this.addEventListener();					
						this.initEmailAutocomplete();					
					}
			};
			var wp = window.pageModel;		
			modelController.init(wp.service, wp.menuModel , wp.loginModel);
			modelController.prepareResource();
		})();		
	</script>		
</body>
</html>