<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
				<span class="sr-only">Toggle navigation</span>
	            <span class="icon-bar"></span>
	            <span class="icon-bar"></span>
	            <span class="icon-bar"></span>
	        </button>
	        <a class="navbar-brand" href="#">美日一句</a>
	    </div>
	    
	    <div class="navbar-collapse collapse" style="height: 1px;">
	     	<ul id="navContainer" class="nav navbar-nav" data-bind="foreach: resource">
	     		<li data-bind="css: {active: '/englishProject/'+url ==  location.pathname}">
	     			<a data-bind="text: name_tw, attr: { href: '/englishProject/'+url }"></a>
	     		</li>
          	</ul>
          	<ul	id="loginContainer" class="navbar-form navbar-right" role="from" >
          	 	<li class="form-group" >
          	 		<c:if test="${isLogin}">
          	 			<input id='accountView' type="text" placeholder="Email" maxlength="40" style="display: none;"
          	 				class="form-control"  data-bind="css: {'sr-only' : isLogIn }">
          	 		</c:if>
          	 		<c:if test="${!isLogin}">
          	 			<input id='accountView' type="text" placeholder="Email" maxlength="40"
          	 				class="form-control"  data-bind="css: {'sr-only' : isLogIn }">
          	 		</c:if>
          	 	</li>            	
            	<li class="form-group">
            		<c:if test="${isLogin}">
            			<input id='pwdView' type="password" placeholder="Password" maxlength="16" style="display: none;"
            				class="form-control"  data-bind="css: {'sr-only' : isLogIn }">
            		</c:if>
            		<c:if test="${!isLogin}">
            		    <input id='pwdView' type="password" placeholder="Password" maxlength="16" 
            				class="form-control"  data-bind="css: {'sr-only' : isLogIn }">
            		</c:if>
            	</li>				             	
            	<li class="form-group">
            		<c:if test="${isLogin}">       		
            			<button id='loginBtn' type="button" class="btn btn-success ladda-button" data-style="zoom-out" >
            				<span class="ladda-label" >Sign out</span>
            			</button>
            		</c:if>
            		 <c:if test="${!isLogin}">       		
            			<button id='loginBtn' type="button" class="btn btn-success ladda-button" data-style="zoom-out" >
            				<span class="ladda-label" >Sign in</span>
            			</button>
            		</c:if>
            	</li>
          	</ul>          	     	
         </div>          
	</div>
</div>