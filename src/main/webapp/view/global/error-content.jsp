<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="jumbotron">
     <div class="container">
       <h1>ERROR!</h1>
       <p>${errorMessage}</p>
     </div>
</div>