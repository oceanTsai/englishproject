<%@ page language="java" contentType="text/html; charset=UTF-8"   pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<spring:eval var="image" expression="@commonProperties.nginx_image"/>
<style>
textarea {
    resize: none;   
}
select {
	margin-top: 10px;
}
.cube{
	border-radius : 0px;
	color: #428bca;
	border-bottom: 0px;
}
.topGap{
	margin-top: 10px;
}
.bottomGap{
	margin-bottom: 10px;
}
</style>

<div id="coreContiner" class="container-fluid" role="main">
	<!-- 英、繁、簡，切換鈕列 -->
	<div class="row">
		<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" style="padding-left: 5px; padding-right: 5px">			
			<div id="enBtn" class="btn btn-lg cube" data-bind="css : enBtnCSS" role="button">英</div>
			<div id="twBtn" class="btn btn-lg cube"	data-bind="css : twBtnCSS" 
				style="margin-left: -4px; margin-right: -4px"  role="button">繁</div>		
			<div id="cnBtn" class="btn btn-lg cube" data-bind="css : cnBtnCSS" role="button">簡</div>									
		</div>		
	</div>	
	<!-- 輸入欄、呈現欄 -->
	<div class="row">
		<!-- 左欄位空間 -->
		<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" 
			style="padding-left: 5px; padding-right: 5px">
			<!-- 輸入欄 -->
			<textarea class="form-control" rows="7" style="font-size: 18px" 
				data-bind="css:{'sr-only' : enAreaisHide }, value : content" ></textarea>					
			<textarea class="form-control" rows="7" style="font-size: 18px"  
				data-bind="css:{'sr-only' : twAreaisHide }, value : context_tw"></textarea>
			<textarea class="form-control" rows="7" style="font-size: 18px"  
				data-bind="css:{'sr-only' : cnAreaisHide }, value : context_cn"></textarea>
			<div class="row topGap bottomGap">
				<!-- 種類下拉選單 -->	
				<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">										
					<select class="form-control navbar-left" style="margin-top: 0px;height: 48px"
						data-bind="options: categoryList, optionsText : 'name_tw', optionsValue: 'category', value:bySelectCategory">												
					</select>					
				</div>
				<!-- 新增 -->
				<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" >
					<button id="insertBtn" class="btn btn-default btn-lg navbar-right"						 
						data-style="zoom-out" data-spinner-color="#428bca" data-bind="css: insertBtnCSS"   
						style="border-radius : 0px;" role="button">
						<span class="glyphicon glyphicon-upload"></span> 新增</button>
						
					<button class="btn btn-default btn-lg navbar-right"  
						data-style="zoom-out" data-spinner-color="#428bca" data-bind="css: updateBtnCSS , click:  cancel"  
						style="border-radius : 0px;" role="button">						
						<span class="glyphicon glyphicon-remove"></span> 取消</button>
						
					<button id="updateBtn" class="btn btn-default btn-lg navbar-right" 
						data-style="zoom-out" data-spinner-color="#428bca" data-bind="css: updateBtnCSS"   
						style="border-radius : 0px;" role="button">
						<span class="glyphicon glyphicon-ok"></span> 更新</button>			
				</div>
			</div>										
		</div>
		<!-- 右欄位空間 -->
		<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
			<!-- 資料列表 -->										
			<table class="table">
			      <thead>
			        <tr>
			          <th class="col-md-1">#</th>
			          <th>詞句</th>
			          <th class="col-md-1"></th>
			        </tr>
			      </thead>
			      <tbody data-bind="foreach: sentenceList">
			        <tr>			 
			          <td data-bind="text : $index"></td>
			          <td>			          	
			          	<article data-bind="text : content"></article>
			      		<article data-bind="text : context_tw"></article>		          	
			          </td>
			          <td><button class="btn btn-default" data-bind="css: $parent.insertBtnCSS , click:  $parent.editClick">Edit</button></td>			          			          			         
			        </tr>			       
			      </tbody>
			 </table>							
		</div>			
	</div>
</div>

<script>
(function(){	
	if(!window.pageModel){
		window.pageModel = {};			
	}
	
	/** 
	 *	每個頁面都會有自己的核心 (統一名稱pageCore)
	 *  統一進入點是 excuted
	 *  所有模組都會在layout處理呼叫流程  (因為資料關係)
	 */
	var pageCore = {
			service : null,
			ko : null,
			container : null,
			viewModel : null,
			block : null,
			listCount : 10,
			//語文輸入切換按鈕
			enBtn : null,
			twBtn : null,
			cnBtn : null,
			insertBtn : null,
			updateBtn : null,
			//滾回頂端
			scrollTarget :  $("html,body"),
			//取分類資料成功
			getCategorySuccess : function(data, textStatus, jqXHR ){
				if( data!=null && data.result!=null ){					
					if(data.result.data){
						this.target.setCategory(data.result.data);
					}
				}
			},
			getCategoryFail : function( jqXHR, textStatus, errorThrown ){				
			},			
			setCategory : function(category){
				this.viewModel.categoryList(category);
			},
			//取最新的n筆List
			getTopSentence : function(count){			 	
				this.service.getTopSentence(this.getTopSentenceSuccess, this.getTopSentenceFail, this, count);
			},
			//取得列表資料成功後，更新列表資料。
			getTopSentenceSuccess : function(data, textStatus, jqXHR){
				if( data!=null && data.result!=null ){					
					if(data.result.data){
						this.target.setSentenceList(data.result.data);
					}
				}
				this.target.block.stopLoading();
			},
			getTopSentenceFail : function(jqXHR, textStatus, errorThrown){	
				this.target.block.stopLoading();
			},
			setSentenceList : function(data){
				this.viewModel.sentenceList(data);
			},
			//加入偵聽
			addBtnEventListener : function(){
				this.enBtn.bind( 'click' , this.enBtnClick );
				this.twBtn.bind( 'click' , this.twBtnClick );
				this.cnBtn.bind( 'click' , this.cnBtnClick );
				this.insertBtn.bind( 'click' , this.insertBtnClick );
				this.updateBtn.bind( 'click' , this.updateBtnClick );
			},
			insertBtnClick : function(){				
				var that = pageCore;				
				var content = that.viewModel.content();
				var context_tw = that.viewModel.context_tw();
				var context_cn = that.viewModel.context_cn();
				var sound = that.viewModel.sound();
				var icon = that.viewModel.icon();
				var category = that.viewModel.bySelectCategory();
				
				if(!content){				
					alert('英文詞句不可以是空白!');
				}else{
					that.block.startLoading();	
					that.service.insertSentence (that.insertSentenceSuccess, that.insertSentenceFail , that,
							content, context_tw ,context_cn, sound, icon, category);
				}
				that = null;				
			},
			//insert 成功
			insertSentenceSuccess : function(data, textStatus, jqXHR){
				if( data!=null && data.result!=null && data.result.success){					
					this.target.getTopSentence(this.target.listCount); 
					this.target.viewModel.cancel();
				}else{
					alert('新增資料失敗');
					this.target.block.stopLoading();
				}					
				this.target = null;
			},
			//insert 失敗
			insertSentenceFail : function(jqXHR, textStatus, errorThrown){
				this.target.block.stopLoading();
				alert('新增資料失敗');
			},		
			updateBtnClick : function(){
				var that = pageCore;	
				var id = that.viewModel.id;
				var content = that.viewModel.content();
				var context_tw = that.viewModel.context_tw();
				var context_cn = that.viewModel.context_cn();
				var sound = that.viewModel.sound();
				var icon = that.viewModel.icon();
				var category = that.viewModel.bySelectCategory();
				
				if(!content){				
					alert('英文詞句不可以是空白!');
				}else{
					that.block.startLoading();
					that.service.updateSentence (that.updateSentenceSuccess, that.updateSentenceFail , that,
							id, content, context_tw ,context_cn, sound, icon, category);					
				}
				that = null;				
			},
			//update 成功
			updateSentenceSuccess : function(data, textStatus, jqXHR){
				this.target.block.stopLoading();
				this.target.viewModel.cancel();
				if(data && data.result && data.result.success ){
					this.target.getTopSentence(this.target.listCount); //取最新詞句n筆	
				}				
			},
			//update 失敗
			updateSentenceFail : function(qXHR, textStatus, errorThrown){
				this.target.block.stopLoading();
				alert("更新失敗!");
			},
			enBtnClick : function(){
				var that = pageCore;
				that.hideAllArea();
				that.viewModel.enAreaisHide(false);
				that = null;			
			},
			twBtnClick : function(){
				var that = pageCore;
				that.hideAllArea();
				that.viewModel.twAreaisHide(false);
				that = null;
			},
			cnBtnClick : function(){
				var that = pageCore;
				that.hideAllArea();
				that.viewModel.cnAreaisHide(false);
				that = null;
			},
			hideAllArea : function(){
				this.viewModel.enAreaisHide(true);
				this.viewModel.twAreaisHide(true);
				this.viewModel.cnAreaisHide(true);
			},
			setResource : function(resource){
				this.viewModel.resource( resource );
			},
			getResource : function(){
				return this.viewModel.resource();
			},		
			//knockoutjs mvvm bind
			excuteBind : function(){					
				this.viewModel = {					
					resource : ko.observableArray(),	 //user, role , resource 資料
					sentenceList : ko.observableArray(), //列表資料
					categoryList : ko.observableArray(), //分類資料
					id : '',
					content : ko.observable(),		 //英文內容
					context_tw : ko.observable(''),		 //繁
					context_cn : ko.observable(''),		 //簡
					sound : ko.observable(''),
					icon : ko.observable(''),
					bySelectCategory : ko.observable(),	 //被選到的分類
					enAreaisHide : ko.observable(false),
					twAreaisHide : ko.observable(true),
					cnAreaisHide : ko.observable(true),
					isEditMode   : ko.observable(false),
					editClick : function(){
						var scroll = window.pageModel.pageCore.scrollTarget;
						var vm = window.pageModel.pageCore.viewModel;						
						vm.isEditMode(true);
						vm.id = this.id;
						vm.content(this.content);
						vm.context_tw(this.context_tw);
						vm.context_cn(this.context_cn);
						vm.sound(this.sound);
						vm.icon(this.icon);
						vm.bySelectCategory(this.category);
						scroll.animate({scrollTop:0},100); //捲回頂端
						vm = null;
					},
					//取消鈕按下
					cancel : function(){
						var vm = window.pageModel.pageCore.viewModel;						
						vm.isEditMode(false);
						vm.id = '';
						vm.content('');
						vm.context_tw('');
						vm.context_cn('');
						vm.sound('');
						vm.icon('');
						vm.bySelectCategory(vm.categoryList()[0].category);
						vm = null;
					}
				};	
				this.viewModel.enBtnCSS = ko.computed(function() {
	        		return this.enAreaisHide()  ? 'navbar-default' : 'btn-success active';
	    		}, this.viewModel);
				
				this.viewModel.twBtnCSS = ko.computed(function() {
	        		return this.twAreaisHide()  ? 'navbar-default' : 'btn-success active';
	    		}, this.viewModel);
				
				this.viewModel.cnBtnCSS = ko.computed(function() {
	        		return this.cnAreaisHide()  ? 'navbar-default' : 'btn-success active';
	    		}, this.viewModel);
				
				this.viewModel.updateBtnCSS = ko.computed(function() {
	        		return this.isEditMode()  ? 'ladda-button' : 'sr-only';
	    		}, this.viewModel);
				
				this.viewModel.insertBtnCSS = ko.computed(function() {
	        		return this.isEditMode()  ? 'sr-only' : 'ladda-button';
	    		}, this.viewModel);
				
				ko.applyBindings( this.viewModel, this.container );				
			},
			//js直譯關係，下面共同框架js取得user role resource資料後，會來呼叫此接口。
			excuted : function(){
				var resource = window.pageModel.resource;
				this.setResource( resource );				
			},
			//初始化
			init : function( ko, container, service, block, enBtn, twBtn, cnBtn, insertBtn, updateBtn){				
				this.ko = ko;				
				this.container = container;
				this.service = service;
				this.block = block;
				this.block.startLoading();
				this.enBtn = enBtn;
				this.twBtn = twBtn;
				this.cnBtn = cnBtn;			
				this.insertBtn = insertBtn;
				this.updateBtn = updateBtn;
				this.excuteBind();//knockoutjs bind
				this.service.getSentenceCategory( this.getCategorySuccess , this.getCategoryFail, this ); //取分類下拉選單資料
				this.getTopSentence(this.listCount); //取最新詞句n筆
				this.addBtnEventListener();
			}
	};
	//mount	
	var coreContiner = document.getElementById('coreContiner');
	var service = window.pageModel.service;
	var block = window.pageModel.blockModel;
	block.init('<div><img src="${image}/spiffygif_3.gif"></div>');
	pageCore.init( window.ko ,coreContiner , service, block, $('#enBtn'), $('#twBtn'), $('#cnBtn'), $('#insertBtn') , $('#updateBtn'));
	window.pageModel.pageCore = pageCore;	
})();
</script>