<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>


<div id="coreContiner" class="jumbotron">
     <div class="container">
       <h1>後台管理功能，施工中!</h1>       
     </div>
</div>


<script>
(function(){	
	if(!window.pageModel){
		window.pageModel = {};			
	}
	
	/** 
	 *	每個頁面都會有自己的核心 (統一名稱pageCore)
	 *  統一進入點是 excuted
	 *  所有模組都會在layout處理呼叫流程  (因為資料關係)
	 */
	var pageCore = {
			service : null,
			ko : null,
			container : null,
			viewModel : null,			
			laddaView : null,	
			setResource : function(resource){
				this.viewModel.resource( resource );
			},
			getResource : function(){
				return this.viewModel.resource();
			},
			//knockoutjs mvvm bind
			excuteBind : function(){					
				this.viewModel = {
					resource : ko.observableArray(),	 //user, role , resource 資料									
				};				
				ko.applyBindings( this.viewModel, this.container );				
			},
			excuted : function(){
				var resource = window.pageModel.resource;
				this.setResource( resource );				
			},
			//初始化
			init : function( ko, container, service ){
				this.service = service;
				this.ko = ko;				
				this.container = container;					
				this.excuteBind();//knockoutjs bind
			}
	};
	//mount	
	var coreContiner = document.getElementById('coreContiner');
	var service = window.pageModel.service;
	pageCore.init( window.ko ,coreContiner , service);
	window.pageModel.pageCore = pageCore;	
})();
</script>