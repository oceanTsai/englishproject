<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<spring:eval var="image" expression="@commonProperties.nginx_image"/>
<spring:eval var="socketio" expression="@commonProperties.nodejs_socketio"/>
<spring:eval var="nodeHost" expression="@commonProperties.nodejs_host"/>
<script src="${socketio}/socket.io.js"></script>
<style>
.randomCube{
	max-width: 340px;	
	height: 60px;
	font-size: 1.5em;
}
.artBlock{
	min-height:360px;
	
	font-size: 1.5em;	
}
.word{
	cursor: pointer;
}
</style>
<div id="coreContiner" class="container-fluid" role="main">	
	<div class="row">		
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="thumbnail artBlock">			    		    	
		    	<div  class="caption" style="margin-top: 1em" >
		    		<article id="enContiner" data-bind="html : en">
		    			
		    		</article>
		    		<br/>		    		
		    		<article data-bind="with : sentence">
		    			<p  data-bind="text : context_tw"></p>
		    		</article>		    		
		    	</div>
		     </div>											
		</div>				
	</div>
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">				     		 	
		    	<button id="randomBtn" type="button" class="randomCube col-xs-12 col-sm-5 col-md-5 col-lg-5  btn btn-primary">換一句</button>		    
		</div>
	</div>		
	
</div>


<script>
(function(){	
	if(!window.pageModel){
		window.pageModel = {};			
	}
	
	/** 
	 *	每個頁面都會有自己的核心 (統一名稱pageCore)
	 *  統一進入點是 excuted
	 *  所有模組都會在layout處理呼叫流程  (因為資料關係)
	 */
	var pageCore = {
			socket : null,
			sessionID : null,	
			service : null,		
			ko : null,			//knockoutjs core
			container : null,	//knockoutjs view
			viewModel : null,	//knockoutjs viewModel
			block : null,		//loading mask model
			enContiner : null,
			randomBtn : null,	
			setEnContiner : function(continer){
				this.enContiner = continer;
				this.enContiner.bind('click', this.wordClickHandler); //bubble
			},
			wordClickHandler : function(e){				
				var self = pageCore;
				if(e.target && e.target.nodeName && e.target.nodeName == 'SPAN'){					
					self.block.startLoading();	
					var request = {
						sessionID : self.sessionID,
						sentenceID : self.viewModel.sentence().id,
						word : $(e.target).text()
					};					
					self.socket.emit('increaseHits', request); 
				}
				self = null;
			},
			increaseHitsReply : function(data){
				var self = pageCore;
				if(data){
					//點擊成功
				}else{
					//點擊失敗
				}
				self.block.stopLoading();
				self = null;
			},
			setRandomBtn : function(btn){
				this.randomBtn = btn;
				this.randomBtn.bind('click', this.randomBtnClick);
			},
			randomBtnClick : function(){
				var self = pageCore;
				self.block.startLoading();	
				self.socket.emit('getRandomSentence', self.sessionID); 
			},	
			addSocketListener : function(){
				this.socket.on('registerReply', this.registerReply );			//註冊回應
				this.socket.on('getSentenceReply', this.getSentenceReply );		//取句子回應
				this.socket.on('increaseHitsReply', this.increaseHitsReply);	//點擊回應
			},
			registerReply : function(data){
				var self = pageCore;					
				if(data){					
					self.socket.emit('getRandomSentence', self.sessionID);
				}else{
					alert('連線失敗');
					self.block.stopLoading();
				}
				self = null;
			},				
			getSentenceReply : function(data){
				var self = pageCore;
				self.block.stopLoading();
				if(data && data.result && data.result.success && data.result.data  ){
					//這是js的切割法，目前切割在java做
					//var enStr = content.match(/[a-zA-z0-9\\']+/g); //英文
					//var symbol = content.match(/[。？！?.! ]+/g);	//符號	
					var content = data.result.data.content;	
					var enList = data.result.data.englistList;
					var symbolList = data.result.data.symbolList;
					var content = "";
					var enSize = enList.length;
					var symbolSize = symbolList.length;
					var endSize = (enSize >= symbolSize) ? enSize : symbolSize;
					for(var i=0 ; i < endSize ; i++){						
						content += "<span class='word'>" + enList[i] + "</span>" + symbolList[i];				
					}
					self.viewModel.en(content);
					self.viewModel.sentence(data.result.data);
				}else{
					alert('取詞失敗!');
				}						  
			},		
			setResource : function(resource){
				this.viewModel.resource( resource );
			},
			getResource : function(){
				return this.viewModel.resource();
			},
			//knockoutjs mvvm bind
			excuteBind : function(){					
				this.viewModel = {
					resource : ko.observableArray(),	 //user, role , resource 資料
					sentence : ko.observable(),
					en : ko.observableArray()
				};				
				ko.applyBindings( this.viewModel, this.container );				
			},
			//js直譯關係，下面共同框架js取得user role resource資料後，會來呼叫此接口。
			excuted : function(){
				var resource = window.pageModel.resource;
				this.setResource( resource );			
			},
			//初始化
			init : function( ko, container, service, block, randomBtn ){				
				this.ko = ko;				
				this.container = container;
				this.service = service;
				this.block = block;
				this.block.startLoading();
				this.setEnContiner($('#enContiner'));
				this.setRandomBtn( randomBtn );
				this.excuteBind();//knockoutjs bind
				this.socket = io.connect(location.host+':3000/sentenceChannel');
				this.sessionID = $.cookie('JSESSIONID');
				this.addSocketListener();//做好socket的偵聽
				this.socket.emit('register', this.sessionID); //註冊一個房間由sessionID分房 
			}
	};
	//mount	
	var coreContiner = document.getElementById('coreContiner');	
	var service = window.pageModel.service;
	var block = window.pageModel.blockModel;
		block.init('<div><img src="${image}/spiffygif_3.gif"></div>');
	pageCore.init( window.ko ,coreContiner , service, block, $('#randomBtn'));	
	window.pageModel.pageCore = pageCore;
})();
</script>