<%@ page language="java" contentType="text/html; charset=UTF-8"    pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<spring:eval var="image" expression="@commonProperties.nginx_image"/>

<div id="coreContiner" class="container-fluid theme-showcase" role="main">
	<div class="row" data-bind="foreach: resource">
		<!-- 解析度占用欄位設定 -->
		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
			<a style="text-decoration: none;"  data-bind="attr: {href : '/englishProject/' + url}">
			   	<div class="thumbnail">			    		    	
			    	<div  class="caption" style="margin-top: 1em">
			    		<span data-bind="attr: { class: glyphicon }" 
				    			style="text-align: center; width:100%;font-size: 3.5em"></span>
			    	</div>	    	
			     	<div class="caption">	     		 	
			       		<h3 data-bind="text : name_tw" class="text-center" ></h3>		       		
			       		<p data-bind="text : description" class="text-center"></p>
			     	</div>
			     </div>
			</a>
		</div>
	</div>
</div>
<script>
(function(){	
	if(!window.pageModel){
		window.pageModel = {};			
	}
	
	/** 
	 *	每個頁面都會有自己的核心 (統一名稱pageCore)
	 *  統一進入點是 excuted
	 *  所有模組都會在layout處理呼叫流程  (因為資料關係)
	 */
	var pageCore = {
			ko : null,			//knockoutjs core
			container : null,	//要綁定的view
			viewModel : null,	//knockoutjs
			block : null,
			viewItems : null,	//畫面上每一個項目
			setviewItems : function(viewItems){
				viewItems.hover( this.doTransition , this.undoTransition);
				//viewItems.bind('mouseenter' , this.doTransition);
			},
			doTransition : function(){	
				$(this).css( "background-color", " #39b3d7" );//#fe57a1 #5bc0de  #eee #39b3d7 
				/* 搖動效果
				for(var i = 0 ; i < 2 ; i++){
				$(this).transition({ y: 2, easing: 'snap',  duration: 100 })
				  .transition({ y: -2 ,easing: 'snap',  duration: 100 }) ;
				}
				$(this).transition({ x: -0 ,easing: 'snap',  duration: 100 }) ;
				*/
				$(this).transition( { y: 5 ,easing: 'snap',  duration: 100 } ) ;
				
			},
			undoTransition : function(){				
				$(this).css( "background-color", "" );
				$(this).transition({ y: 0 });				  
			},
			setResource : function(resource){				
				this.viewModel.resource( resource );
				this.block.stopLoading();
			},
			getResource : function(){
				return this.viewModel.resource();
			},
			excuteBind : function(){				
				this.viewModel = {
					resource : ko.observableArray()
				};				
				ko.applyBindings( this.viewModel, this.container );
			},
			//js直譯關係，下面共同框架js取得user role resource資料後，會來呼叫此接口。
			excuted : function(){
				var resource = window.pageModel.resource;
				this.setResource( resource );				
				this.setviewItems( $('.thumbnail') );
			},
			//如同建構子
			init : function( ko, container, block){
				this.block = block;
				this.block.startLoading();
				this.ko = ko;				
				this.container = container;				
				this.excuteBind();				
			}
	};
	//mount
	var block = window.pageModel.blockModel;
		block.init('<div><img src="${image}/spiffygif_3.gif"></div>');
	pageCore.init( window.ko , document.getElementById('coreContiner'), block);
	window.pageModel.pageCore = pageCore;
})();
</script>