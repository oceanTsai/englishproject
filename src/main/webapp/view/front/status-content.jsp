<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<spring:eval var="image" expression="@commonProperties.nginx_image"/>
<spring:eval var="socketio" expression="@commonProperties.nodejs_socketio"/>
<spring:eval var="nodeHost" expression="@commonProperties.nodejs_host"/>
<script src="${socketio}/socket.io.js"></script>
<!-- 統計頁面 -->
<style>
.artBlock{
	font-size: 1.5em;	
}
.word{
	cursor: pointer;
}
</style>
<div id="coreContiner" class="container-fluid" role="main">	
	<div class="row" id="enContiner" data-bind="foreach: wordList" >		
		<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
			<div class="thumbnail artBlock" data-bind="click : $parent.wordClick">		    	
	    		<article>
	    			<span class="word" data-bind="text : word"></span>:
	    			<span data-bind="text : value[0]"></span>		    			
	    		</article>
		     </div>											
		</div>				
	</div>	
</div>
<script>
(function(){	
	if(!window.pageModel){
		window.pageModel = {};			
	}
	
	/** 
	 *	每個頁面都會有自己的核心 (統一名稱pageCore)
	 *  統一進入點是 excuted
	 *  所有模組都會在layout處理呼叫流程  (因為資料關係)
	 */
	var pageCore = {
			socket : null,
			sessionID : null,	
			service : null,		
			ko : null,			//knockoutjs core
			container : null,	//knockoutjs view
			viewModel : null,	//knockoutjs viewModel
			block : null,		//loading mask model

			//sockic 回應頻道偵聽
			addSocketListener : function(){
				this.socket.on('registerReply', this.registerReply );		//註冊回應
				this.socket.on('getWordListReply', this.getWordListReply );	//取現在閱讀頁的句子的回應
				this.socket.on('clearHitsReply', this.clearHitsReply);	//清除點擊的回應
				this.socket.on('hitChangedReply', this.hitChangedReply);
			},
			hitChangedReply : function(success){
				var self = pageCore;
				if(success){
					self.registerReply(success);
				}
				self = null;
			},
			registerReply : function(data){
				var self = pageCore;					
				if(data){					
					self.socket.emit('getWordList', self.sessionID);
				}else{
					alert('連線失敗');
					self.block.stopLoading();
				}
				self = null;
			},
			sortWord : function(wordList){
				wordList.sort(function(a,b){						
					if(a.value[1] < b.value[1]){
						return -1;
					}
					if(a.value[1] > b.value[1]){
						return 1;
					}
					return 0;
				});				
			},
			getWordListReply : function(data){
				var self = pageCore;
				self.block.stopLoading();
				if(data && data.result && data.result.success && data.result.data  ){
					var wordList = [];
					var source = data.result.data;
					for(var key in source){						
						var item = {
							word : key,
							value : source[key]
						};
						wordList.push(item);
					}
					//排序
					self.sortWord(wordList);
					/*
					wordList.sort(function(a,b){						
						if(a.value[1] < b.value[1]){
							return -1;
						}
						if(a.value[1] > b.value[1]){
							return 1;
						}
						return 0;
					});*/
					self.setWordList(wordList);
				}				
				self = null;
			},
			clearHitsReply : function(data){
				var self = pageCore;
				//self.block.stopLoading();
				self.getWordListReply(data);
		
				self = null;
			},
			setWordList : function(data){
				this.viewModel.wordList(data);
			},
			setResource : function(resource){
				this.viewModel.resource( resource );
			},
			getResource : function(){
				return this.viewModel.resource();
			},
			//knockoutjs mvvm bind
			excuteBind : function(){					
				this.viewModel = {
					resource : ko.observableArray(),	 //user, role , resource 資料
					wordList : ko.observableArray(),
					wordClick : function(){
						var self = pageCore;
						self.block.startLoading();
						var request = {
								sessionID : self.sessionID,
								word : this.word
						};				
						self.socket.emit('clearHits', request);//送出清除命令
						self = null;
					}
				};
				ko.applyBindings( this.viewModel, this.container );				
			},
			//js直譯關係，下面共同框架js取得user role resource資料後，會來呼叫此接口。
			excuted : function(){
				var resource = window.pageModel.resource;
				this.setResource( resource );			
			},
			//初始化
			init : function( ko, container, service, block ){				
				this.ko = ko;				
				this.container = container;
				this.service = service;
				this.block = block;
				this.block.startLoading();
				this.excuteBind();//knockoutjs bind
				this.socket = io.connect(location.host+':3000/sentenceChannel');
				this.sessionID = $.cookie('JSESSIONID');
				this.addSocketListener();//做好socket的偵聽
				this.socket.emit('register', this.sessionID); //註冊一個房間由sessionID分房 
			}
	};
	//mount	
	var coreContiner = document.getElementById('coreContiner');	
	var service = window.pageModel.service;
	var block = window.pageModel.blockModel;
		block.init('<div><img src="${image}/spiffygif_3.gif"></div>');
	pageCore.init( window.ko ,coreContiner , service, block );	
	window.pageModel.pageCore = pageCore;
})();
</script>